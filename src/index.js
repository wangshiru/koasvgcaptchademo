// const koa = require('koa')
import koa from 'koa'
// node的path模块
// const path = require('path')
import path from 'path'
const app = new koa()
// const helmet = require('koa-helmet')
import helmet from 'koa-helmet'
// 静态资源，是绝对路径
// const statics = require('koa-static')
import statics from 'koa-static'

// const router = require('./routes/routes')
import router from './routes/routes'

import koaBody from 'koa-body'
import jsonutil from 'koa-json'
import cors from '@koa/cors'
import compose from 'koa-compose'
import compress from 'koa-compress'

const isDevMode = process.env.NODE_ENV === 'production' ? false : true

// app.use(helmet())
// 访问 http://localhost:3000/logo.jpeg 即可加载图片
// app.use(statics(path.join(__dirname, '../public')))

// 使用 koa-compose 集成中间件
const middleware = compose([
    koaBody(),
    statics(path.join(__dirname, '../public')),
    cors(),
    jsonutil({pretty: false, param: 'pretty'}),
    helmet()
])
// 生产模式下，数据压缩
if(isDevMode){
    app.use(compress())
}

app.use(middleware)

app.use(router())

app.listen(3000)