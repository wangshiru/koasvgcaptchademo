// 路由压缩
import combineRoutes from 'koa-combine-routers'

import PublicRouter from './PublicRouter'

export default combineRoutes(PublicRouter)