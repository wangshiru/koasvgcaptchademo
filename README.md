# 简介

使用 Koa 框架结合 svg-captcha 实现简单的验证码服务

# 运行

```
npm install
npm run dev
```

# 打包

```
npm run build
```

# 前端引入

```html
<div class="layui-form-mid layui-word-aux svgCaptcha" v-html="this.svgCaptcha" @click="getCaptcha">
```

前端封装 getCaptcha 函数，每次点击图片后重新请求数据，服务端返回新的 svg 图片，在 vue 中直接使用 v-html 可以渲染 svg xml

```js
getCaptcha () {
    axios.get('http://localhost:3000/getCaptcha').then((res) => {
    console.log(res)
    if (res.data.code === 200) {
        // data 为验证码 svg xml
        this.svgCaptcha = res.data.data
        // text 为验证码答案
        this.code = res.data.text
    }
    })
}
```
